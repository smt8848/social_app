// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/




// function showHideComments(e) {
//   var x = document.getElementById(`myDIV${e}`);
//   if (x.style.display === "none") {
//     document.getElementById(`show${e}`).innerHTML = "Hide";
//     x.style.display = "block";
//   } 
//   else {
//        document.getElementById(`show${e}`).innerHTML = "Show";
//       x.style.display = "none";
//     }
// }

function profileImageClick(){
  $('#profile_image_input').click();
}

function imageInputChange(){
   var name = document.getElementById('profile_image_input'); 
   let formData = new FormData();
   formData.append('image', name.files[0]);
   $.ajax({
    method: 'POST',
    type: 'POST',
    url: '/upload_profile_pic',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    success: function(data){
        console.log(data);
    }
  });
}

function iconAppear(){
  $("#icon_appear").css("opacity", "1");
}

function iconDisappear(){
  $("#icon_appear").css("opacity", "0");
}