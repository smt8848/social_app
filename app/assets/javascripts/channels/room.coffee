jQuery(document).on 'turbolinks:load', ->
    App.room = App.cable.subscriptions.create { channel: "RoomChannel" },
    received: (data) ->
      $messages = $('#messages')
      $messages.append(data.html)
      $messages.scrollTop $messages.prop('scrollHeight')
      $('#message_notifiaction'+data.friend_id).empty().append(data.notification)

    connected: ->
      # Called when the subscription is ready for use on the server

    disconnected: ->
      # Called when the subscription has been terminated by the server

    speak: (message, friend_id) ->
      @perform 'speak', message: message, friend_id: friend_id


    #, n_count: n_count