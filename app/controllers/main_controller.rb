class MainController < ApplicationController

  def search
    @users = User.ransack(full_name_or_email_cont: params[:q]).result(distinct: true)
    @emails = User.ransack(email_cont: params[:q]).result(distinct: true)
    respond_to do |format|
      format.html {}
      format.json{
        @users = @users.limit(5)
        @emails = @emails.limit(5)
      }
    end  
  end

end