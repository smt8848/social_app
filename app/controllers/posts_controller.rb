class PostsController < ApplicationController
  def new
    @post = Post.new
  end

  def index
    @posts = []
    if user_signed_in?
      @posts << current_user.posts if current_user.posts
      current_user.friends.each do |friend|
        @posts << friend.posts
      end
    else
      @posts << Post.all.order(created_at: :desc)
    end
    @posts.flatten!.sort_by!{|e| -e[:created_at].to_i}

  end

  def profile
    @user = User.find(params[:id])
    @posts = []
      @posts << @user.posts if @user.posts
      @posts.flatten!.sort_by!{|e| -e[:created_at].to_i}
  end
  
  def destroy
    Post.find(params[:id]).destroy   
      redirect_to posts_path
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update 
      post = Post.find(params[:id])
      if post.update(permit_post)
        if params[:post][:image]
          params[:post][:image].each do |pic|
            img = Pic.new(permit_image)
            img.post_id = params[:id]
            img.image = pic
            img.save
          end
        end
        flash[:success] = "Success"
        redirect_to post_path(post)
      else
        flash[:error] = post.errors.full_messages
        redirect_to new_post_path
      end
  end 

  def show
      @post = Post.find(params[:id])
  end

  def create
    @post = Post.new(permit_post)
    @post.user_id = current_user.id
      if @post.save
        params[:post][:image].each do |pic|
          img = Pic.new(permit_image)
          img.post_id = @post.id
          img.image = pic
          img.save
        end  
        flash[:success] = "Success"
        redirect_to post_path(@post)
      else
        flash[:error] = @post.errors.full_messages
        redirect_to new_post_path
      end  
  end

#TO prevent sql injection, ensures user can only edit image(s) and descriptions only.  
  private 
    def permit_post
       params.require(:post).permit(:description) 
    end

    def permit_image
       params.require(:post).permit(:image) 
    end
 

end