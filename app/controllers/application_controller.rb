class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :incomming_events, if: :user_signed_in?

  def test
  end


  protected

    def incomming_events
      @friend_requests = FriendRequest.where(friend_id: current_user.id)
      @incomming_messages = Message.where(friend_id: current_user.id, is_read: false)
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:full_name])
      #devise_parameter_sanitizer.for(:account_update) << :name
    end

end
