class UsersController < ApplicationController


  def upload_profile_pic
    current_user.image = params[:image]
    current_user.save
  end

  def delete_profile_pic
    current_user.image.destroy
    current_user.save
  end

  def update_profile
    current_user.age = params[:select_age]
    current_user.gender = params[:select_gender]
    current_user.save
  end

  def edit_profile
  end

  def update_name
    current_user.full_name = params[:full_name]
    current_user.save
  end

end