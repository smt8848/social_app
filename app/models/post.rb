# frozen_string_literal: true

# Model for Posts
class Post < ApplicationRecord
  belongs_to :user
  has_many :pics, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
end