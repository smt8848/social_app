json.users do
  json.array!(@users) do |user|
    json.name user.full_name
    json.url user_profile_path(user)
    end
  end

  json.emails do
  json.array!(@emails) do |mail|
    json.name mail.email
    json.url user_profile_path(mail)
    end
  end