class BroadcastMessageJob < ApplicationJob
  queue_as :default

  def perform(message)
     ActionCable.server.broadcast 'room_channel',html: render_message(message), notification: User.find(message.user_id).messages.where(is_read: false).count, friend_id: message.friend_id
  end

  private
  def render_message(message)
    ApplicationController.renderer.render message
  end

end
